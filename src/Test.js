import React, { Component } from 'react';

class Test extends Component {
    submit() {
        var strAnswer= "";
        var strFromServer = null;
        strAnswer = "https://libraries.io/api/" + this.platform.value + "/" + this.name.value;
        var xhr = new XMLHttpRequest();
        xhr.open('GET', strAnswer, false);
        xhr.send();
        if (xhr.status !== 200) {
            alert( xhr.status + ': ' + xhr.statusText );
          } else {
            alert( xhr.responseText ); 
          }
        strFromServer = JSON.parse(xhr.responseText);
        this.platform.value = strFromServer.homepage;
        this.name.value = strFromServer.repository_url;

    };

    render() {
        return (
            <div>
            <p><input name=":platform" ref={(input) => this.platform = input} type="Text" /></p>
            <p><input name=":name" ref={(input) => this.name = input} type="Text" /></p>
            <form name="formRequest" ref={(form) => this.form = form}>
                <input type="submit" onClick={this.submit.bind(this)} />
            </form>
            </div>
        );
    }
}

export default Test;